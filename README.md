# Kerr_Resonator_Animations


## Description
This project, associated with the manuscript /Spectral kissing and its dynamical consequences in the squeezed Kerr-nonlinear oscillator/, includes both the data used in the different manuscript figures and a set of videos in `mp4` format that show the dynamics of the Husimi quasidistribution for a selected set of inital coherent states (see Fig. 2a in the manuscript) evolving under a Hamiltonian that includes a nonlinear Kerr oscillator plus a squeezing drive. These videos are part of the supplementary material of the manuscript entitled.

All included animations are computed with a basis of 1201 Fock states, and a xi parameter, ratio of the squeezing drive amplitude over the Kerr amplitude, xi = 180, the same value used as an example in the manuscript. See the manuscript draft in arXiv for further info.

The included animations filenames  are `Husimi_Coherent_regular_istate_<state>_Ne_1_N_1200_xi_180.mp4`, where `state = O, A, B, C, D, E` using paper's notation.


## Authors and acknowledgment

The manuscript authors are Jorge Chávez-Carlos (Instituto de Ciencias Nucleares, UNAM, México), Talía L. M. Lezama (Yeshiva University, USA), Rodrigo G. Cortiñas (Yale University, USA), Jayameenakshi Venkatraman (Yale University, USA), Michel H. Devoret (Yale University, USA), Víctor S. Batista (Yale University, USA), Francisco Pérez-Bernal (Universidad de Huelva, Spain), and Lea F. Santos (University of Connecticut-Storrs, USA).

The responsible person for this project is Curro Pérez-Bernal

## License

[Creative Commons Attribution 4.0 International license (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/)

## Project status
This is an ongoing project. Expect future changes.






